extern crate rand;

use std::io;
use rand::Rng;
use std::cmp::Ordering;

fn main() {
    println!("Guess the number!");
    
    let secret_number = rand::thread_rng().gen_range(1,101);
   
    println!("The secret number is {}", secret_number);
    
    let mut num_guess = 0;     
 
    loop {

        println!("Please input your guess: ");	
    
        let mut guess = String::new();
    
    
        io::stdin().read_line(&mut guess)
	    .expect("Failed to read line");
	
        let guess: u32 = match guess.trim().parse()
	{    
	    Ok(num) => num,
	    Err(_)  => {
		println!("Input not a number, try again");
		continue;
	    }    	
	};
	num_guess = num_guess + 1;    	
    
        println!("You Guessed {}", guess);
        println!("Number of guesses: {}",num_guess); 
        match guess.cmp(&secret_number) 
        { // Fix input like 09 and define borders so no numbers >100
            Ordering::Less    => println!("Too small"),
	    Ordering::Greater => println!("Too big"),
	    Ordering::Equal   => {
		println!("You win. You guessed {} times", num_guess);
		break;
	    }
    	}
    
    }

}
